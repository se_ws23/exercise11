package sort

import org.openjdk.jmh.annotations.*

import java.util.concurrent.TimeUnit
import collection.immutable.ArraySeq
import scala.util.Random

@BenchmarkMode(Array(Mode.AverageTime))
@OutputTimeUnit(TimeUnit.MICROSECONDS)
@Warmup(iterations = 10, time = 200, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 40, time = 100, timeUnit = TimeUnit.MILLISECONDS)
@Fork(1)
@State(Scope.Benchmark)
class ArraySort:
  @Param(Array("1000", "10000", "100000"))
  var size: Int = _

  var array: ArraySeq[Int] = _

  @Setup
  def fillArray(): Unit = array = ArraySeq.fill(size)(Random.nextInt())

  @Benchmark
  def sortAscending(): ArraySeq[Int] = array.sorted

  @Benchmark
  def sortDescending(): ArraySeq[Int] = array.sortWith(_>_)